<?php

namespace Drupal\commerce_shipping_extra_weight_rate\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Drupal\physical\MeasurementType;
use Drupal\physical\Weight;


/**
 * Provides the ShippingExtraWeightRate shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "commerce_shipping_extra_weight_rate",
 *   label = @Translation("Shipping extra weight rate"),
 * )
 */
class ShippingExtraWeightRate extends ShippingMethodBase {

  /**
   * Constructs a new ShippingExtraWeightRate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->services['default'] = new ShippingService('default', $this->configuration['rate_label']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'rate_label' => '',
      'rate_description' => '',
      'base_rate_amount' => NULL,
      'minimum_weight' => NULL,
      'extra_weight_rate' => NULL,
      'services' => ['default'],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $amount = $this->configuration['base_rate_amount'];
    // A bug in the plugin_select form element causes $amount to be incomplete.
    if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
      $amount = NULL;
    }
    
    $minimum_weight = $this->configuration['minimum_weight'];
    if (!isset($minimum_weight) || !isset($minimum_weight['number'], $minimum_weight['unit'])) {
      $minimum_weight = NULL;
    } else {
      $minimum_weight = ['number' => $this->configuration['minimum_weight']['number'], 'unit' => $this->configuration['minimum_weight']['unit']];
    }

    $extra_weight_rate = $this->configuration['extra_weight_rate'];
    // A bug in the plugin_select form element causes $amount to be incomplete.
    if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
      $extra_weight_rate = NULL;
    }

    $form['rate_label'] = [
      '#type' => 'textfield',
      '#title' => t('Rate label'),
      '#description' => t('Shown to customers when selecting the rate.'),
      '#default_value' => t($this->configuration['rate_label']),
      '#required' => TRUE,
    ];
    $form['rate_description'] = [
      '#type' => 'textfield',
      '#title' => t('Rate description'),
      '#description' => t('Provides additional details about the rate to the customer.'),
      '#default_value' => t($this->configuration['rate_description']),
    ];
    $form['base_rate_amount'] = [
      '#type' => 'commerce_price',
      '#title' => t('Base rate amount'),
      '#default_value' => $amount,
      '#required' => FALSE,
    ];
    $form['minimum_weight'] = [
      '#type' => 'physical_measurement',
      '#measurement_type' => MeasurementType::WEIGHT,
      '#title' => $this->t('Minimum weight'),
      '#default_value' => $minimum_weight,
      '#required' => TRUE,
    ];
    $form['extra_weight_rate'] = [
      '#type' => 'commerce_price',
      '#title' => t('Extra weight rate'),
      '#description' => t('Rate per unit weight above minimum weight'),
      '#default_value' => $extra_weight_rate,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['rate_label'] = $values['rate_label'];
      $this->configuration['rate_description'] = $values['rate_description'];
      $this->configuration['base_rate_amount'] = $values['base_rate_amount'];
      $this->configuration['minimum_weight'] = $values['minimum_weight'];
      $this->configuration['extra_weight_rate'] = $values['extra_weight_rate'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    $weight = $shipment->getWeight();
    if (!$weight) {
      return FALSE;
    }
    
    $conf = $this->configuration;

    // assume default weight unit (Kg for now)
    // Perhaps need to get store default weight
    $weight_unit = $conf['minimum_weight']['unit'];
    $min_weight_obj = new Weight($conf['minimum_weight']['number'], $conf['minimum_weight']['unit']);
    $minimum_weight = $min_weight_obj->convert($weight_unit)->getNumber();

    $weight = $weight->convert($weight_unit)->getNumber();

    if ($weight <= $minimum_weight) {
      $shipping_price = $conf['base_rate_amount']['number'];
    } else {
      $shipping_price = $conf['base_rate_amount']['number'] + ($weight - $minimum_weight) * $conf['extra_weight_rate']['number'];
    }

    // Set shipping amount.
    $amount = new Price((string) $shipping_price, $conf['base_rate_amount']['currency_code']);

    $rounder = \Drupal::service('commerce_price.rounder');
    $amount = $rounder->round($amount);

    $rates = [];
    $rates[] = new ShippingRate([
      'shipping_method_id' => $this->parentEntity->id(),
      'service' => $this->services['default'],
      'amount' => $amount,
      'description' => $this->configuration['rate_description'],
    ]);

    return $rates;
  }

}
