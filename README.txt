CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Similar modules
 * Installation
 * Configuration
 * Instructions
 * Maintainers


INTRODUCTION
-------------

Commerce shipping extra weight formula considers only base charge if order weight 
is lower or equal to minimum weight. It applies extra weight rate for any excess 
weight above minimum.  Some greek shipping companies use this model of charging 
for (i.e. ACS).


For example lets say we set these:
Base rate 5 euro
Minimum weight 3 kg
Extra weight rate 1 euro 

If order is below 3 kg shipping will cost 5 euro
If order is 5 kg shipping will cost 5 euro + (5 kg - 2 kg) x 1 = 7 euro


REQUIREMENTS
-------------

Drupal Commerce 2.x - https://www.drupal.org/project/commerce_shipping
Commerce Shipping 2.x - https://www.drupal.org/project/commerce_shipping
Physical fields - https://www.drupal.org/project/physical

SIMILAR MODULES
-------------

https://dgo.to/commerce_shipping_weight_tariff module differs in that the charges are calculated in steps.  Always considering 
higher weight to calculate shipping charges.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.


CONFIGURATION
-------------

The module configuration is similar to default commerce shipping method configuration.


INSTRUCTIONS
-------------

To use this shipping method just try to follow next steps:
1. Create a new shipping method here:
   "/admin/commerce/config/shipping-methods/add"
2. Select "Shipping by weight formula" plugin.
3. Enter plugin label.
4. Set base rate amount for shipping method.
5. Add minimum weight amount (below this weight base rate applies)
6. Add extra weight rate (applied to extra weight above minimum)

Other shipping method options you can configure as neeeded.


MAINTAINERS
-----------

Current maintainers:
 * Giorgos Kontopoulos (giorgosk) - https://www.drupal.org/u/giorgosk
